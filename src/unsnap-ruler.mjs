let shiftKey = false;

const onKeyEvent = (ev) => {
	if (ev.isComposing) return;
	shiftKey = ev.shiftKey;
}

Hooks.once('ready', () => {
	// TODO: Maybe replace with control manager usage?
	['keydown', 'keyup']
		.forEach(e => window.addEventListener(e, onKeyEvent, { passive: true }));

	console.log('UNSNAP RULER | TAKING DIRECT CONTROL!');

	/* global GridLayer */
	// const layer = CONFIG.Canvas.layers?.grid?.layerClass ?? GridLayer;

	// OVERRIDE
	GridLayer.prototype.getCenter = function (x, y) {
		return canvas.controls.ruler?._state > 0 && shiftKey ? [x, y] : this.grid.getCenter(x, y);
	}

	// Alternative implementation would override
	// - Ruler.measure
	// - Ruler._addWaypoint
});
